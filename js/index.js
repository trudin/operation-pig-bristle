/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        var el = document.getElementById('myApp');
        angular.bootstrap(el, ['mainModule']);
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

//Tvingar fram ett 'deviceready' event. Behövs för att sidan ska laddas i desktop browser.
window.setTimeout(function() {
    var e = document.createEvent('Events'); 
    e.initEvent("deviceready", true, false); 
    document.dispatchEvent(e);
}, 50);

document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        document.addEventListener("backbutton", function (e) {
            e.preventDefault();
        }, false );
}

var mainModule = angular.module("mainModule", ["ngRoute", "firebase"]);
var firebaseRef = new Firebase("https://spelappen.firebaseio.com/");
var mapSize = 4;
var unwatchTurn = function() {};

mainModule.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/", {
        templateUrl: "html/login.html"
    });
    $routeProvider.when("/myhouse", {
        templateUrl: "html/myhouse.html"
    });
    $routeProvider.when("/createcharacter", {
        templateUrl: "html/createcharacter.html"
    });
    $routeProvider.when("/customizecharacter", {
        templateUrl: "html/customizecharacter.html"
    });
    $routeProvider.when("/bountyshop", {
        templateUrl: "html/bountyshop.html"
    });
    $routeProvider.when("/town", {
       templateUrl: "html/town.html" 
    });
    $routeProvider.when("/stats", {
       templateUrl: "html/stats.html" 
    });
    $routeProvider.when("/highscore", {
       templateUrl: "html/highscore.html" 
    });
    $routeProvider.when("/sleep", {
       templateUrl: "html/sleep.html" 
    });    
    $routeProvider.when("/shop", {
       templateUrl: "html/shop.html" 
    });
    $routeProvider.when("/square", {
       templateUrl: "html/square.html" 
    });
    $routeProvider.when("/fight", {
       templateUrl: "html/fight.html" 
    });
    $routeProvider.when("/aftermath", {
        templateUrl: "html/aftermath.html"
    });
    $routeProvider.when("/bountyshop", {
        templateUrl: "html/bountyshop.html"
    });
    $routeProvider.otherwise({
        redirectTo: "/"
    });
}]);

mainModule.controller("content-controller", ["$scope", "$routeParams", "$location", "$timeout", "$firebaseAuth", "$firebaseObject", "$firebaseObject", "$firebaseArray", "$firebaseArray",
    function($scope, $routeParams, $location, $timeout, $firebaseAuth, $firebaseObject, $currentSquare, $playerArray, $challengeQueue){

    $scope.setSquare = function(loc) {

        $scope.player.state = "roaming";
        $scope.playerArrayNotReady = true;
        
        if($scope.playerArray === undefined) {
            
            $scope.square = $currentSquare(firebaseRef.child("map")
                .child($scope.player.currentSquare.x + "")
                .child($scope.player.currentSquare.y + ""));
            $scope.playerArray = $playerArray(firebaseRef.child("map")
                .child($scope.player.currentSquare.x + "")
                .child($scope.player.currentSquare.y + "").child("playerArray"));
        }
        
        $scope.playerArray.$remove($scope.playerArray.$indexFor($scope.playerKey));
        
        if(loc === "north") {
            $scope.player.currentSquare.x = $scope.player.currentSquare.x - 1;
        } else if(loc === "south") {
            $scope.player.currentSquare.x = $scope.player.currentSquare.x + 1;
        } else if(loc === "west") {
            $scope.player.currentSquare.y = $scope.player.currentSquare.y - 1;
        } else if(loc === "east") {
            $scope.player.currentSquare.y = $scope.player.currentSquare.y + 1;
        }
        
        if($scope.player.currentSquare.x <= 0) {
            $scope.disableNorth = true;
        } else {
            $scope.disableNorth = false;
        }
        
        if($scope.player.currentSquare.y <= 0) {
            $scope.disableWest = true;
        } else {
            $scope.disableWest = false;
        }
                
        if($scope.player.currentSquare.x >= mapSize-1) {
            $scope.disableSouth = true;
        } else {
            $scope.disableSouth = false;
        }
        
        if($scope.player.currentSquare.y >= mapSize-1) {
            $scope.disableEast = true;
        } else {
            $scope.disableEast = false;
        }
        
        if($scope.player.currentSquare.x === 2 && $scope.player.currentSquare.y === 1) {
            $scope.setLocation("town");
        } else {
            $location.path("square");
        }
        
        $scope.square = $currentSquare(firebaseRef.child("map")
            .child($scope.player.currentSquare.x + "")
            .child($scope.player.currentSquare.y + ""));
        $scope.playerArray = $playerArray(firebaseRef.child("map")
            .child($scope.player.currentSquare.x + "")
            .child($scope.player.currentSquare.y + "").child("playerArray"));
        $scope.playerArray.$add({uid: firebaseRef.getAuth().uid, name: $scope.player.name, isDead: $scope.player.isDead, hasBounty: $scope.player.hasBounty}).then(function(ref) {
            $scope.playerKey = ref.key();
            $scope.playerArrayNotReady = false;
            firebaseRef.child(ref.path.toString()).onDisconnect().remove();
        });

        $scope.square.$loaded().then(function() {

            $scope.msg = "";                                    // prepare this now for use in switch cases and ng-show, square.html

            var r     = 100*Math.random(100);                   // used for switch statement
            var value = Math.floor((Math.random() * 10) + 1);   // prepared to be used in switch cases if appropiate

            $scope.randomEvent = true;                          // always true, used for switch and ng-show

            if(!$scope.player.isDead) {
                switch($scope.randomEvent) {

                case (r < 10)           : $scope.msg = "A random zombie bites you for "+value+" damage.";         takeDamageNoKill(value,$scope.player); break;
                case (r > 9  && r < 20) : $scope.msg = "You trip over your own feet for "+1+" damage.";           takeDamageNoKill(1,$scope.player); break;
                case (r > 19 && r < 30) : $scope.msg = "You pick an apple and eat it, healing "+value+" points."; takeDamageNoKill(-value,$scope.player); break;
                case (r > 29 && r < 40) : $scope.msg = "You hear the birds twitter."; break;
                case (r > 39 && r < 50) : $scope.msg = "You look around and see no one looking so you prance through the woods."; break;
                case (r > 49 && r < 60) : $scope.msg = "You find "+value+" goldfish on the ground."; $scope.player.goldfish += value; break;

                default: $scope.msg = "You walk through the woods."; break;
                }
            } else {
                $scope.msg = "You float around like a restless soul";
            }                        
            

            $scope.player.xp += 1;
            $scope.player.$save();
        });
    };
    
    $scope.disableNorth = false;
    $scope.disableSouth = false;
    $scope.disableWest  = false;
    $scope.disableEast  = false;
    
    $scope.watchChallenged = function() {
        $scope.unwatchPlayerState = $scope.playerState.$watch(function() {
            if ($scope.playerState.$value === "challenged") {
                console.log("Challenged");
                $scope.playerArray.$ref().child($scope.playerKey).child("disabled").set(true);
                $scope.enemyPlayer = $firebaseObject(firebaseRef.child("players").child($scope.player.enemyPlayer));
                $scope.enemyPlayerUid = $scope.player.enemyPlayer;
                $scope.enemyPlayer.$loaded().then(function() {
                    console.log($scope.enemyPlayer);
                    console.log("Redirecting to fight");
                    $location.path("/fight");
                    $scope.player.state = "fighting";
                    $scope.player.turn = false;
                    $scope.player.$save();
                });
            }
        });
    };
    
    $scope.setLocation = function(loc, playerUid) {
        $scope.enemyPlayerUid = playerUid;
        var fight = false;

        /*if (loc === "sleep") {
            $scope.restRecover = 10;

            if ($scope.player.hp + $scope.restRecover > $scope.player.maxhp) {
                $scope.restRecover = $scope.player.maxhp - $scope.player.hp;
            }
            $scope.player.hp += $scope.restRecover;
            $scope.player.$save();
        } else*/ if (loc === "fight") {
            
        $scope.unwatchPlayerState();
        fight = true;
        challenger = true;
        $scope.playerArray.$ref().child($scope.playerKey).child("disabled").set(true);
        $scope.player.$loaded().then(function() {
            $scope.player.state = "fighting";
            $scope.player.turn = true;
            $scope.player.$save();
            $scope.enemyPlayer = $firebaseObject(firebaseRef.child("players").child(playerUid));
            $scope.enemyPlayer.$loaded().then(function() {
                $scope.enemyPlayer.state = "challenged";
                $scope.enemyPlayer.$save();
                var challengeQueue = $challengeQueue(firebaseRef.child("playerQueue").child(playerUid));
                challengeQueue.$loaded(function() {
                    challengeQueue.$add({player: $scope.authData.uid}).then(function(){
                        $timeout(function(){
                            if(challengeQueue[0].player === $scope.authData.uid) {
                                angular.forEach(challengeQueue, function(challenge) {
                                   challengeQueue.$remove(challenge); 
                                });
                                $scope.enemyPlayer.enemyPlayer = $scope.authData.uid;
                                $scope.enemyPlayer.$save();
                                $location.path(loc);
                            } else {
                               
                                $scope.playerArray.$ref().child($scope.playerKey).child("disabled").set(false);
                            }
                        }, 1200);
                    });
                });
            });
        });
            
        } else if (loc === "town") {
            $scope.player.currentSquare.x = 2;
            $scope.player.currentSquare.y = 1;
            $scope.player.state = "inTown";
            $scope.player.$save();
        }
        if(!fight) {
            $location.path(loc);
        }
    };
    
    $scope.login = function() {

        if ($scope.loginForm.$valid) {
            firebaseRef.authWithPassword({
                email    : $scope.email,
                password : $scope.password
            }, function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                    alert(error.message);
                } else {
                    console.log("Authenticated successfully with payload:", authData);

                    $scope.player = $firebaseObject(firebaseRef.child("players").child(authData.uid));

                    $scope.player.$loaded().then(function() {
                        if ($scope.player.name) {
                            $scope.player.state = "inTown";
                            $scope.player.$save().then(function() {
                                $location.path("/myhouse");
                            });
                        }
                    });
                }
            });
        } else {
            console.log("Invalid input!");
        }
    };

    $scope.logout = function() {
        firebaseRef.unauth();
        $location.path("/");
    };

    $scope.register = function(event) {

        event.preventDefault();

        if ($scope.loginForm.$valid) {
            firebaseRef.createUser({
                email    : $scope.email,
                password : $scope.password
            }, function(error, userData) {
                if (error) {
                    console.log("Error creating user:", error);
                } else {
                    $scope.userData = userData;
                    console.log("Successfully created user account with uid:", userData.uid);
                    $scope.login();
                    $timeout(function () {
                        $location.path("/createcharacter");
                    }, 0);
                }

            });
        } else {
            console.log("Invalid input!");
        }
    };

    $scope.auth = $firebaseAuth(firebaseRef);

    $scope.watchXP = function() {
        $scope.playerXP.$watch(function() {
            if ($scope.player.xp >= $scope.calculateXpNeededForLevel($scope.player.lvl + 1)) {
                $scope.levelUp();
            }
        });
    };
    
    $scope.watchState = function() {

        $scope.playerState.$watch(function() {
           
            if ($scope.playerState.$value === "roaming" || $scope.playerState.$value === "dead") {
                if ($scope.playerKey) {
                    $scope.playerArray.$ref().child($scope.playerKey).child("disabled").remove();
                }
                unwatchTurn();
                console.log("roaming");
                $location.path("/square");
                $scope.watchChallenged();
            } else if ($scope.playerState.$value === "lost" || $scope.playerState.$value === "won") {
                $location.path("/aftermath");
            }
        });
    };

    //Körs varje gång man loggar in eller laddar om sidan
    $scope.auth.$onAuth(function(authData) {
        $scope.authData = authData;
        if ($scope.authData) {
            $scope.player = $firebaseObject(firebaseRef.child("players").child($scope.authData.uid));
            $scope.playerState = $firebaseObject(firebaseRef.child("players").child($scope.authData.uid).child("state"));
            $scope.playerEnemy = $firebaseObject(firebaseRef.child("players").child($scope.authData.uid).child("enemyPlayer"));
            $scope.playerXP = $firebaseObject(firebaseRef.child("players").child($scope.authData.uid).child("xp"));

            $scope.watchXP();
            $scope.watchChallenged();
            $scope.watchState();

            $scope.playerEnemy.$watch(function() {
                console.log("enemy player: " + $scope.playerEnemy.$value);
                $scope.enemyPlayer = $firebaseObject(firebaseRef.child("players").child($scope.playerEnemy.$value));
            });
        } else {
            $scope.player = null;
        }
    });


    $scope.levelUp = function() {

        $scope.player.$loaded().then(function() {
            $scope.player.lvl += 1;
            $scope.player.points += (2 + Math.floor($scope.player.lvl / 2));
            $scope.player.maxhp += Math.ceil($scope.player.maxhp * 0.15);
            $scope.player.hp = $scope.player.maxhp;
            $scope.player.$save();
            if (window.cordova) {
                navigator.notification.alert("Grattis! Du levlade precis upp! Du är nu level " + $scope.player.lvl + ", din Max HP är " + $scope.player.maxhp + " och du har " + $scope.player.points + " points att dela ut!");
            } else {
                alert("Grattis! Du levlade precis upp! Du är nu level " + $scope.player.lvl + ", din Max HP är " + $scope.player.maxhp + " och du har " + $scope.player.points + " points att dela ut!");
            }
        });
    };

    $scope.calculateXpNeededForLevel = function(level) {
        if (level === 1) return 0;
        return 100 * Math.ceil(Math.pow((level), 2.8));
    };
}]);

mainModule.controller("createCharacterCtrl", ["$scope", "$location", "$timeout", function($scope, $location, $timeout){

    $scope.points = 5;
    $scope.strength = 0;
    $scope.toughness = 0;

    $scope.subtractPoint = function(property) {
        if (property.match("strength") && $scope.strength > 0) {
            $scope.points += 1;
            $scope.strength -= 1;
        }
        if (property.match("toughness") && $scope.toughness > 0) {
            $scope.points += 1;
            $scope.toughness -= 1;
        }
    };
    $scope.addPoint = function(property) {
        if (property.match("strength") && $scope.points > 0) {
            $scope.points -= 1;
            $scope.strength += 1;
        }
        if (property.match("toughness") && $scope.points > 0) {
            $scope.points -= 1;
            $scope.toughness += 1;
        }
    };

    $scope.createCharacter = function(event) {

        event.preventDefault();

        if ($scope.createCharacterForm.$valid && $scope.points === 0) {
            $scope.player = new Player($scope.characterName);
            $scope.player.strength = $scope.strength;
            $scope.player.toughness = $scope.toughness;

            firebaseRef.child("players").child($scope.authData.uid).set($scope.player, function(error){

                if (error) {
                    console.log("Error");
                } else {
                    // Time out necessary otherwise must hit login button twice to move to location
                    $timeout(function () {
                        $location.path("/myhouse");
                    }, 0);   
                }
            });
        } else {
            console.log("Invalid input!");
        }
    };
}]);

mainModule.controller("customizeCharacterCtrl", ["$scope", "$location", "$timeout", function($scope, $location, $timeout){

    $scope.showWeapons = false;
    $scope.showShields = false;
    $scope.showArmor = false;

    $scope.showWeaponsMenu = function() {
        $scope.showWeapons = !$scope.showWeapons;
        $scope.showShields = false;
        $scope.showArmor = false;
    };

    $scope.showShieldsMenu = function() {
        $scope.showShields = !$scope.showShields;
        $scope.showWeapons = false;
        $scope.showArmor = false;
    };

    $scope.showArmorMenu = function() {
        $scope.showArmor = !$scope.showArmor;
        $scope.showWeapons = false;
        $scope.showShields = false;
    };

    $scope.equipWeapon = function(item) {
        var temp = $scope.player.equippedWeapon;
        $scope.player.equippedWeapon = item;
        $scope.player.inventory.splice($scope.player.inventory.indexOf(item), 1, temp);
        $scope.player.$save();
    };

    $scope.equipShield = function(item) {
        var temp = $scope.player.equippedShield;
        $scope.player.equippedShield = item;
        $scope.player.inventory.splice($scope.player.inventory.indexOf(item), 1, temp);
        $scope.player.$save();
    };

    $scope.equipArmor = function(item) {
        var temp = $scope.player.equippedArmor;
        $scope.player.equippedArmor = item;
        $scope.player.inventory.splice($scope.player.inventory.indexOf(item), 1, temp);
        $scope.player.$save();
    };

    $scope.player.$loaded().then(function() {
        $scope.playerLoaded = true;
        var playerStrength = $scope.player.strength;
        var playerToughness = $scope.player.toughness;

        $scope.subtractPoint = function(property) {
            if (property.match("strength") && $scope.player.strength > playerStrength) {
                $scope.player.points += 1;
                $scope.player.strength -= 1;
            }
            if (property.match("toughness") && $scope.player.toughness > playerToughness) {
                $scope.player.points += 1;
                $scope.player.toughness -= 1;
            }
        };

        $scope.addPoint = function(property) {
            if (property.match("strength") && $scope.player.points > 0) {
                $scope.player.points -= 1;
                $scope.player.strength += 1;
            }
            if (property.match("toughness") && $scope.player.points > 0) {
                $scope.player.points -= 1;
                $scope.player.toughness += 1;
            }
        };
    });

    $scope.saveCharacter = function() {
        $scope.player.$loaded().then(function() {
            $scope.player.$save();
            $location.path("/myhouse");
        });
    };
}]);

mainModule.controller("highScoreCtrl", ["$scope", "$firebaseArray", function($scope, $firebaseArray) {
    $scope.players = $firebaseArray(firebaseRef.child("players"));
}]);

mainModule.controller("fightingCtrl", ["$scope", "$firebaseObject", "$location", "$interval", "$firebaseArray",
function($scope, $firebaseObject, $location, $interval, $bountyArray) {

    $scope.playerTurn = $firebaseObject(firebaseRef.child("players").child($scope.authData.uid).child("turn"));
    $scope.bountyArray = $bountyArray(firebaseRef.child("bounties"));
    var stop;
    $scope.showMagic = false;
    $scope.showInventory = false;
    $scope.player.$loaded().then(function() {
        $scope.player.fightLog = [{"text" : "Fight Log"}];
    });

    unwatchTurn = $scope.playerTurn.$watch(function() {
        console.log("turn watch");
        if ($scope.playerTurn.$value) {
            console.log("turn value: "+$scope.playerTurn.$value);
            navigator.vibrate(2000);
            $scope.timeLeft = 15;
            $scope.startCount();
        }
    });

    $scope.startCount = function() {
        stop = $interval(function() {
            $scope.timeLeft -= 1;
            if ($scope.timeLeft <= 0) {
                $scope.pushFightLogObject({"text" : "Turen gick över till " + $scope.enemyPlayer.name + " eftersom " + $scope.player.name + " tog för lång tid på sig!"});
                $scope.stopCount();
                $scope.endTurn();
            }
        }, 1000);
    };
    
    $scope.stopCount = function() {
        $interval.cancel(stop);
    };

    $scope.hitEnemy = function(input) {

        var damageDealt = calculateDamage(attack($scope.player), $scope.enemyPlayer);

        if (takeDamage(damageDealt, $scope.enemyPlayer)) {
            $scope.pushFightLogObject({"text" : $scope.player.name + " attackerade med " + $scope.player.equippedWeapon.name + " och "
                                        + "dödade " + $scope.enemyPlayer.name  + "!"});
            $scope.endFight();
        } else {
            $scope.pushFightLogObject({"text" : $scope.player.name + " attackerade med " + $scope.player.equippedWeapon.name + ". "
                                        + $scope.enemyPlayer.name  + " förlorade " + damageDealt + " HP!"});
            $scope.endTurn();
        }
        $scope.stopCount();
    };

    $scope.endFight = function() {
        $scope.playerState.$value = "won";
        $scope.player.state = "won";
        $scope.player.xp += $scope.enemyPlayer.lvl * 100;
        $scope.playerState.$save();
        $scope.enemyPlayer.state = "lost";
        $scope.enemyPlayer.isDead = "true";
        var loot = Math.ceil($scope.enemyPlayer.goldfish / 2);
        $scope.enemyPlayer.goldfish -= loot;
        $scope.enemyPlayer.$save();
        $scope.player.goldfish += loot;
        $scope.player.$save();

        if ($scope.enemyPlayer.hasBounty === true) {
            var goldfish = 0;
            angular.forEach($scope.bountyArray, function(bounty) {
               if (bounty.uid === $scope.enemyPlayerUid) {
                   goldfish += bounty.price;
                   $scope.bountyArray.$remove(bounty);
               } 
            });
            $scope.enemyPlayer.hasBounty = false;
            $scope.enemyPlayer.$save();
            $scope.player.goldfish += goldfish;
            $scope.player.$save();
        }
    };

    $scope.showInventoryMenu = function() {
        $scope.showInventory = !$scope.showInventory;
        $scope.showMagic = false;
    };

    $scope.showMagicMenu = function() {
        $scope.showMagic = !$scope.showMagic;
        $scope.showInventory = false;
    };

    $scope.useItem = function(item) {
        if (item.type === "Potion" && $scope.player.hp < $scope.player.maxhp) {
            $scope.hpRecover = item.hp;
            if ($scope.player.hp + $scope.hpRecover > $scope.player.maxhp) {
                $scope.hpRecover = $scope.player.maxhp - $scope.player.hp;
            }
            $scope.player.hp += $scope.hpRecover;
            if ($scope.player.hp > $scope.player.maxhp) {
                $scope.player.hp = $scope.player.maxhp;
            }
            $scope.pushFightLogObject({"text" : $scope.player.name + " drack en stärkande dryck och återställde " + $scope.hpRecover + " HP!"});
            $scope.removeItemFromInventory(item);
            $scope.player.$save();
            $scope.endTurn();
            $scope.stopCount();
        } else if ((item.type === "Potion")) {
            if (window.cordova) {
                navigator.notification.alert("Du har redan full HP!");
                return;
            } else {
                alert("Du har redan full HP!");
                return;
            }
        }
    };


    $scope.useMagic = function(item) {
        var damageDealt = Math.ceil((Math.random() * 100) + 1);
        if (takeDamage(damageDealt, $scope.enemyPlayer)) {
            $scope.pushFightLogObject({"text" : $scope.player.name + " kastade " + item.name + ", " + $scope.enemyPlayer.name + 
                                    " dog!" });
            $scope.endFight();
        } else {
            $scope.pushFightLogObject({"text" : $scope.player.name + " kastade " + item.name + ", " + $scope.enemyPlayer.name + 
                                    " förlorade " + damageDealt + " HP!"});
            $scope.endTurn();
        } 
        $scope.removeItemFromInventory(item);
        $scope.stopCount();
    };

    $scope.removeItemFromInventory = function(item) {
        $scope.player.inventory.splice($scope.player.inventory.indexOf(item), 1);
        $scope.player.$save();
    };

    $scope.endTurn = function() {
        $scope.showMagic = false;
        $scope.showInventory = false;
        $scope.enemyPlayer.turn = true;
        $scope.enemyPlayer.$save();
        $scope.player.turn = false;
        $scope.player.$save();
    };

    $scope.pushFightLogObject = function(item) {
        $scope.player.fightLog.unshift(item);
        $scope.enemyPlayer.fightLog.unshift(item);
    };

    $scope.leaveFight = function() {
        $scope.stopCount();
        $scope.player.state = "roaming";
        $scope.player.enemyPlayer = "none";
        $scope.player.$save();
        $scope.enemyPlayer.state = "roaming";
        $scope.enemyPlayer.enemyPlayer = "none";
        $scope.enemyPlayer.$save()
    };
}]);

//Butiken
mainModule.controller("butikCtrl", ["$scope", "$firebaseArray", "$firebaseArray", "$firebaseObject", 
function($scope, $bountyArray, $playerArray, $chosenPlayer) {

    //Instans av butik.js
    $scope.affes = new Butik();
    $scope.bounty = 0;
    $scope.buttonText = "Lägg till bounty";
    $scope.bounties = $bountyArray(firebaseRef.child("bounties"));
    $scope.players = $playerArray(firebaseRef.child("players"));
    
    $scope.addBounty = function() {
        if ($scope.showPlayers === true) {
            $scope.showPlayers = false;   
            $scope.buttonText = "Lägg till bounty";
        } else if ($scope.showGoldfish === true) {
            $scope.showGoldfish = false;
            $scope.buttonText = "Lägg till bounty";
        } else {
            $scope.showGoldfish = false;
            $scope.showPlayers = true;        
            $scope.buttonText = "Avbryt";
        }
    };

    $scope.addPlayerBounty = function(player) {
        $scope.chosenPlayer = player;
        $scope.showPlayers = false;
        $scope.showGoldfish = true;
    };

    $scope.addToBounty = function() {
        if ($scope.bounty + 10 > $scope.player.goldfish) {
        } else {
            $scope.bounty += 10;
        }
    };

    $scope.subtractToBounty = function() {
        if ($scope.bounty < 10) {
        } else {
            $scope.bounty -= 10;
        }
    };

    $scope.setBounty = function() {
       $scope.bounties.$add({
       name: $scope.chosenPlayer.name, 
       price: $scope.bounty, 
       uid: $scope.players.$keyAt($scope.chosenPlayer)});
       $scope.player.goldfish -= $scope.bounty;
       $scope.player.$save();
       console.log($scope.players.$keyAt($scope.chosenPlayer));
       var index = $scope.players.$indexFor($scope.players.$keyAt($scope.chosenPlayer));
       $scope.players[index].hasBounty = true;
       $scope.players.$save(index);
       $scope.addBounty();
    };

    $scope.affes.buyStuff = function(item){
        console.log("Buystuff");
        if($scope.player.goldfish >= item.price ){
            $scope.player.goldfish = $scope.player.goldfish - item.price;
            console.log("push");
            if (!$scope.player.inventory) {
                $scope.player.inventory = [];
            }
            $scope.player.inventory.push(item);
            $scope.player.$save();
            alert(item.name + " Added to inventory, you have: " + $scope.player.goldfish + " goldfish left");
             console.log("Added");
        }
        else{
            $scope.affes.randomDiss();   
        }  
    };   
}]);

mainModule.controller("sleepCtrl", ["$scope", "$interval", function($scope, $interval) {
    var stop;
    $scope.startCount = function() {
        stop = $interval(function() {
            $scope.player.$loaded().then(function() {
                if ($scope.player.hp < $scope.player.maxhp) $scope.player.hp += 1;
                if ($scope.player.hp > 0) $scope.player.isDead = false;
                if ($scope.player.hp >= $scope.player.maxhp) $scope.stopCount();
            });
        }, 300);
    };

    $scope.startCount();

    $scope.stopCount = function() {
        $interval.cancel(stop);
    };

    $scope.leave = function() {
        $scope.stopCount();
        $scope.player.$loaded().then(function() {
            $scope.player.$save();
        });
        $scope.setLocation('myhouse');
    };
}]);

mainModule.controller("aftermathCtrl", ["$scope", "$location", "$timeout", function($scope, $location, $timeout) {
    unwatchTurn();
    $timeout(function() {
        $scope.player.$loaded().then(function() {
        if ($scope.player.isDead) {
            $scope.playerArray.$ref().child($scope.playerKey).child("isDead").set(true);
        }
            $scope.playerArray.$ref().child($scope.playerKey).child("hasBounty").set($scope.player.hasBounty);
        });
    }, 1000); 
      
    $scope.leave = function() { 
        challenger = false;
        $scope.player.$loaded().then(function() {
            $scope.player.fightLog = [{"text" : "Fight Log"}]; 
            $scope.player.state = "roaming";   
            $scope.player.$save().then(function() {
                $location.path("/square");
            });
        });
    };
}]);
