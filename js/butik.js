function Item(name, type, description, price, strength, hp, toughness, img) {
	this.name = name;
	this.type = type;
	this.description = description;
	this.price = price;
	this.strength = strength;
	this.hp = hp;
	this.toughness = toughness;
	this.img = "img/" + img;
}

function Butik() {
	
	this.spellCardDragonFire = 50;
	this.spellCardBlackMamba = 100;
	this.spellCardGreenMamba = 100;
	
	this.randomDiss = function() {
		
    var x = Math.floor((Math.random() * 10) + 1);
	
	if(x === 1){alert("Not enough GoldFish");};
	if(x === 2){alert("HAHHAHA U MUST BE HIGH OR SOMETHING!");};
	if(x === 3){alert("Hmmm is that all you have");};
	if(x === 4){alert("Teleport yourself out of here!");};
	if(x === 5){alert("Go back to the woods where you belong!");};
	if(x === 6){alert("PERKELE! ");};
	if(x === 7){alert("You need more Goldfish!");};
	if(x === 8){alert("Don't cry like a little girl.");};
	if(x === 9){alert("How can a mother love that face?");};
	if(x === 10){alert("I can sell you to the circus!");};
    
	}
	
	this.items = [
		//Vapen
		new Item("Rostfri Dolk", "Vapen","Blank och skinande dolk. Ser riktigt vass ut", 300, 5, 0, 0, "goldDagger.png"),
		new Item("Spear", "Vapen","Detta spjut är tillverkat av Britta spjut och bedazzled med lila diamant.", 700, 10, 0, 0, "spear.png"),
		new Item("Dolk i guld", "Vapen","Dolk i vackert skinande guld. Frågan är vad man betalar för egentligen?", 700, 5, 0, 0, "goldDagger.png"),
		new Item("Kort svärd", "Vapen","Kort, vasst och lätthanterligt.", 1200, 15, 0, 0, "goldDagger.png"),

		//Sköldar
		new Item("Järnarmerad träsköld","Shield", "Håller ihop betydligt bättre än de flesta hemgjorda sköldar.", 300, 0, 0, 5, "shield.png"),
		new Item("Vikingasköld","Shield", "Rund, liten sköld i stål.", 700, 0, 0, 10, "shield.png"),
		new Item("Sköld i stål","Shield", "Smidd i ett stycke solitt stål. Imponerande!", 1200, 0, 0, 15, "shield.png"),
		new Item("Utomjordisk Sköld","Shield", "Denna sköld är tillverkad i de djupaste skogarna på månen.", 2000, 0, 0, 20, "shield.png"),

		//Armor
		new Item("Rustning i papper","Armor", "Helt klart ett steg upp från ingen rustning alls.", 300, 0, 0, 5, "armorofgod.png"),
		new Item("Rustning i skinn","Armor", "Den här rustningen är tillverkd i skinn som är både lätt och starkt.", 700, 0, 0, 10, "armorofgod.png"),
		new Item("Rustning i järn","Armor", "Ingen riktig riddare skulle ge sig ut i strid iklädd den här rustningen. Men för en glad amatör så...", 1200, 0, 0, 15, "armorofgod.png"),
		new Item("Rustning i stål","Armor", "Nu börjar det verkligen likna nåt. Vackert blank är den här rustningen.", 2000, 0, 0, 20, "armorofgod.png"),
		new Item("Riddarrustning","Armor", "Passar endast ett fullblod från kungens privata eskort.", 3000, 0, 0, 25, "armorofgod.png"),
		new Item("Armor of God","Armor", "Guds glömda rustning gör dig nästan odödlig.", 100000, 0, 0, 500, "armorofgod.png"),
		
		//Potion
		new Item("Potion*10","Potion", "Fyller på din hälsa med 10.", 50, 0, 10, 0, "potion10.png"),
		new Item("Potion*50","Potion", "Fyller på din hälsa med 50.", 100, 0, 50, 0, "potion10.png"),
		new Item("Potion*100","Potion", "Fyller på din hälsa med 100.", 200, 0, 100, 0, "potion50.png"),
		new Item("Potion*500","Potion", "Fyller på din hälsa med 500.", 400, 0, 500, 0, "potion50.png"),
		
		//Magi
		new Item("Mc.Choke","Magi", "Kramar livet ur din fiende.", 100, 0, 0, 0, "choke.png"),
		new Item("MC.Random","Magi","Ge en random skada mellan 1 och 100.", 100, 0, 0, 0, "random.png"),
		new Item("MC.ZombieBite","Magi", "Tuggar din fiende till en klump köttfärs.", 100, 0, 0, 0, "zombiebite.png"),
		new Item("Teleport", "Magi","Teleportera dig själv hem när du vill.", 100, 0, 0, 0, "teleport.png"),
		new Item("Snoop Dog", "Magi","Förvirra din fiende med grön rök.", 100, 0, 0, 0, "snoopDog.png"),
		new Item("Thunder","Magi", "Stek din fiende med 10 blixtrar.", 100, 0, 0, 0, "thunderBolt.png"),
		
		//PowerUps
		new Item("Styrka*10","Power up", "Fyller på din styrka med 10.", 400, 10, 0, 0, "styrka.png"),
		new Item("Styrka*20", "Power up","Fyller på din styrka med 20.", 800, 20, 0, 0, "styrka.png"),
		new Item("Tuffhet*10","Power up", "Fyller på din tuffhet med 10.", 400, 0, 0, 10, "Toughness.png"),
		new Item("Tuffhet*20","Power up", "Fyller på din tuffhet med 20.", 800, 0, 0, 20, "Toughness.png"),];
};