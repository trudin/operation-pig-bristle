function Player(name) {
    this.name = name;//Användarnamnet
    this.maxhp = 100; // Kan ändras när man levlar
    this.hp = 100;//Spelarens hälsa
    this.xp = 0;//Spelarens experince points
    this.lvl = 1;// Spelarens level
    this.points = 0;
    this.strength = 0;//Spelarens styrka
    this.toughness = 0;//Spelarens tuffhet
    this.inventory = [new Item("Potion*10","Potion", "Fyller på din hälsa med 10.", 100, 0, 10, 0, "potion10.png")];//Spelarens ägodelar
    this.isDead = false;//Om spelaren är död eller inte
    this.goldfish = 20;//Spelarens valuta
    this.currentSquare = {x: 2, y: 1}; //Spelarens nuvarande position
    this.hasBounty = false; //Om spelaren har bounty på sig eller inte  //TODO metod
    this.state = "none";
    this.fightLog = ["Fight Log"];
    this.equippedWeapon = new Item("Rostig Dolk", "Vapen","En gammal dolk du hittat i skogen", 0, 1, 0, 0, "rustydagger.png");
    this.equippedShield = new Item("Sköld av trä", "Shield","En sköld som du snickrat ihop själv", 0, 0, 0, 1, "woodenshield.png");
    this.equippedArmor = new Item("Gammal rock", "Armor","Din gamla nattsärk", 0, 0, 0, 0, "oldrobe.png");
}

    //Attacks the enemy
    function attack(player) {
        var damageToDeal = 10;
        for (var i = 0; i < player.strength + player.equippedWeapon.strength; i++) {
            damageToDeal += Math.floor((Math.random() * 3) + 1);
        }
        console.log("Damage dealt: " + damageToDeal);
        return damageToDeal;
    };

    function calculateDamage(damageDealt, enemy) {
        console.log("Damage dealt is " + damageDealt);
        var toughness = enemy.toughness + enemy.equippedShield.toughness + enemy.equippedArmor.toughness;
        var damage = Math.ceil(damageDealt - (toughness / 2));
        console.log("Damage taken is " + damage);
        if (damage < 1) {
            damage = 1;
        }
        return damage;
    };

    function takeDamage(damage, enemy) {
        enemy.hp -= damage;
        if (enemy.hp <= 0) {
            enemy.hp = 0;
            return true;
        } else {
            return false;
        };
    };
    
    function takeDamageNoKill(damage, thisPlayer) {

        thisPlayer.hp -= damage;

        if(thisPlayer.hp < 0)   thisPlayer.hp = 1;

        if(thisPlayer.hp > thisPlayer.maxhp) thisPlayer.hp = thisPlayer.maxhp;

    };

    // //Har tar spelaren skada
    // this.takeDamage = function(damage) {
    //     //Om spelarens hp är mindre än 1 dör spelaren
    //     if(this.hp - damage < 1){
    //         this.hp = 0;
    //         this.isDead = true;
    //         //TODO:SPAWNS BACK TO PLAYER'S HOUSE 
    //     }
    //     //Annars tar spelaren bara skada
    //     else {
    //         this.hp -= damage;
    //     };
    // };
    
    // //visar stats
    // this.getStats = function() {
    //     return this.name + this.hp + this.xp + this.lvl;
    // };
    
    // //lägger till grejer i din inventory
    // this.addToInventory = function(stuff) {
    //     this.inventory.push(stuff);
    // };
    
    // //Ta bort grejer från inventory
    // this.removefromInventory = function(x) {
    //     this.inventory.splice(x);
    // };
    
    // //Loopar igenom vad spelaren har i sitt inventory
    // this.checkInventory = function() {
    //     for(var a in this.inventory) {
    //         console.log(this.inventory[a]);
    //     };
    // };
    
    // //När spalaren handlar i butiken dras gf från spelarens goldfishes
    // this.pay = function(gf) {
    //     this.goldfish = this.goldfish - gf;
    //     return this.goldfish;
    // };
    
    // //Ökar din förmögenhet med antal guldfiskar
    // this.addGoldfish = function(gf) {
    //     this.goldfish = this.goldfish + gf;
    //     return this.goldfish;
    // };
    
    // //Fyller på spelarens hp
    // this.addHp = function(tanka) {
    //     this.hp = this.hp + tanka;
    //     return this.hp;
    // };
    
    // //Ger spelaren xp
    // this.giveXp = function(newXp) {
    //     this.xp += newXp;
    //     //Om xp är större än 50 och mindre än 99 så går spelaren upp 1 level.
    //     if(this.xp > 50 && this.xp < 99 ){
    //         this.lvl = this.lvl + 1;
    //         console.log("Yeah du gick up en level");
    //         // todo do mer
    //     }
    // };
    
    // //Ger spelaren styrka
    // this.getStrength = function() {
    //     //Ökar spelarens styrka med 1 varje gång spelaren går upp en level
    //     if(this.lvl > 1 && this.lvl < 2)
    //     {
    //         this.strength = this.strength + 5;
    //     }
    //     else if(this.lvl > 2 && this.lvl < 3)
    //     {
    //         this.strength = this.strength + 5;
    //     };
    //     //todo gör mer
    // };
